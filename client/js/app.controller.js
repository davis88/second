(function () {
    "use strict";
    angular.module("BirthCertApp")
        .controller("BirthCertAppCtrl", BirthCertAppCtrl, ['BirthCertAPPServiceAPI', '$http'])
        
        //['BirthCertAppServiceAPI', '$http']
    
     //This is a standard angular practice to inject a custom service or default service
     // This a controller, it must have an array
     // Because a controller can have many dependencies / services
     // step 1 - of injection process 

    // BirthCertAppCtrl.$inject = ['BirthCertAPPServiceAPI', '$http'];

    function BirthCertAppCtrl(BirthCertAPPServiceAPI, $http) {
        var self  = this;
        
        self.onReset = onReset;
        self.onSubmit = onSubmit;
        self.initForm = initForm;
        
        self.user = {}
        self.isSubmitted = false;
        self.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        self.NRIC = /^[A-Za-z][0-9]{7}[A-Za-z]$/;
        

        self.bloodTypes = [
            { name: "O" , value:0},
            { name: "A", value: 1},
            { name: "B", value: 2},
            { name: "O-", value: 3},
            { name: "A-", value: 4},
            { name: "B-", value: 5},
            { name: "AB", value: 6},
            { name: "AB-", value: 7},     
        ];



        function onReset(){
            self.user = Object.assign({}, self.user);
            self.applicationform.$setPristine();
            self.applicationform.$setUntouched();
        }

        function onSubmit(){
            // console.log(BirthCertAPPServiceAPI);
            BirthCertAPPServiceAPI.register(self.user)
                .then((result)=>{
                    self.user = result;
                    console.log(result);
                    self.isSubmitted = true
                }).catch((error)=>{
                    console.log(error);
                })
        }

        function initForm(){
            self.user.selectedBloodType = "0";
            self.isSubmitted = false;
        }
        
        self.initForm();
    }    

})();  
    